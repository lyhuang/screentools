package com.hly.screentools

import android.app.AlertDialog
import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


    }

    fun showResolution(view: View) {
        var dm = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(dm)
        var dialog = Dialog(this)
        var msg = StringBuilder();
        msg.append("density：").append(dm.density).append("\n\n")
        msg.append("densityDpi：").append(dm.densityDpi).append("\n\n")
        msg.append("heightPixels：").append(dm.heightPixels).append("\n\n")
        msg.append("widthPixels：").append(dm.widthPixels).append("\n\n")
        msg.append("scaledDensity：").append(dm.scaledDensity).append("\n\n")
        msg.append("xdpi：").append(dm.xdpi).append("\n\n")
        msg.append("ydpi：").append(dm.ydpi).append("\n\n")
        var d = AlertDialog.Builder(this).setMessage(msg.toString()).create().show()
    }
}
